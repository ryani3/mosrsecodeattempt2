#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"
#include "assign02.pio.h"
#include "assign02.h"
#include "ANSI-color-codes.h"
#include "txt_to_print.h"
#include "LED.h"
#include "morse.h"

uint8_t current_level, current_lives, num_correct_in_level, num_total_correct, num_total_incorrect, character_to_input;
bool correct;

// Must declare the main assembly entry point before use.
extern void main_asm(); // The main function to start on boot
extern void wait_for_input(); // Wait to receive a new input from the user
extern char *return_input(); // To request the new input given

// Functions for the main assembly to use.
void asm_gpio_init(uint pin) { gpio_init(pin); } // Start GPIO pin
void asm_gpio_set_dir(uint pin, bool out) { gpio_set_dir(pin, out); } // Set direction
bool asm_gpio_get(uint pin) { return gpio_get(pin); } // Get value from pin
void asm_gpio_put(uint pin, bool value) { gpio_put(pin, value); } // Put value in pin
void asm_gpio_set_irq(uint pin) { gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL | GPIO_IRQ_EDGE_RISE, true); } // Set IRQ
void asm_watchdog_update() { watchdog_update(); } // Watchdog timeout function

// Printing
void user_input();  // Prompt for user input
void print_stats(); // Print statistics
void print_color_from_lives(); // Change text color based on current_lives
void print_level_num(); // Print the level number

// Level handling
void select_level(); // User chooses the level
void level_one(); // Level one win condition
void level_two(); // Level two win condition
void level_three(); // Level three win condition
void level_four(); // Level four win condition
bool is_correct(); // Check if win condition met
void play_level(); // Play a level
void play_levels(); // Play each level until you lose

bool are_the_same(int a[4],  int b[4]);

int main() {
    stdio_init_all(); // Initialise stdio
    printf("\n");

    srand(time(0)); // Random seed

    start_LED();    // Initialise the LED
    LED_color_blue(); // Start color blue

    main_asm();     // Initialize the assembly code

    print_welcome_screen();
    printf("%s", txt_instructions);    // Show the instructions

    watchdog_enable(16777215, 0);     // Enable the watchdog

    select_level();    // Choose the current_level to play
    play_levels();    // Play the levels
    print_stats();   // Print end of game statistics

    return (0);
}

void user_input() {
    watchdog_update();
    printf(CYN);
    printf(">>>\t");
    printf(BLU);
    wait_for_input();
    watchdog_update();
    printf(COLOR_RESET);
    printf("\n");
}

void print_stats() {
    int percentageCorrect = ((num_total_correct * 100) / (num_total_correct + num_total_incorrect));
    printf(MAG);
    printf("\t-----------------------------------------\n");
    printf("\t | Total Correct Inputs: %d \t\t|\n", num_total_correct);
    printf("\t | Total Incorrect Inputs: %d \t\t|\n", num_total_incorrect);
    printf("\t | Accuracy: %d%%\t\t\t|\n", percentageCorrect);
    printf("\t-----------------------------------------\n");
}

bool is_correct() {
    if (correct) {
        printf(BLU "\tCorrect!\n" COLOR_RESET);
        return 1;
    }
    else {
        printf(RED "\tIncorrect!!!\n" COLOR_RESET);
        return 0;
    }
}

void print_color_from_lives(){
    switch (current_lives)
    {
    case 3:
        printf(GRN);
        break;
    case 2:
        printf(YEL);
        break;
    case 1:
        printf(RED);
        break;
    case 0:
        printf(RED);
    default:
        printf(GRN);
        break;
    }
}

void print_level_num() {
    printf(txt_level);
    switch (current_level)
    {
    case 4:
        printf(txt_four);
        break;
    case 3:
        printf(txt_three);
        break;
    case 2:
        printf(txt_two);
        break;
    case 1:
        printf(txt_one);
        break;
    default:
        printf("Level ???");
        break;
    }
    printf(GRN "\n" "\t===================================================\n" COLOR_RESET);
}

void select_level() {
    while (current_level == 0) {
        printf("%s\n", txt_select_level);
        user_input();
        watchdog_update();

        int index = morse_to_index(return_input());
        if (index < 1 || index > 4) {
            printf("Not an option. Try Again\n");
        }
        else (current_level = index);
    }
}

void level_one(){
    character_to_input = rand() % 35;
    printf(GRN "Type the following:\t\"" MAG "%s" GRN "\"\nIn Morse Code:\t\t\"" MAG "%s" GRN "\"\n", morse_corr_table[character_to_input], other_morse_table[character_to_input]);
    user_input();
    correct = find_character_from_iter(return_input());
}

void level_two(){
    character_to_input = rand() % 35;
    printf(GRN "Type the following:\t\"" MAG "%s" GRN "\"\nNo Morse Code here :)\n", morse_corr_table[character_to_input]);
    user_input();
    correct = find_character_from_iter(return_input());
}

bool are_the_same(int a[4],  int b[4]) {
    for (int i = 0; i < 4; i++) {
        if (a[i] != b[i]) return false;    
    }
    return true;
}

void level_three(){
    const char *word = four_letter_words[(rand() % 500)];
    int expected_index[4];
    for (int i = 0; i < 4; i++) {
        expected_index[i] = -1;
        for (int j = 0; j < 35; j++) {
            if (*morse_corr_table[j] == word[i]) {
                expected_index[i] = j;
                break;
            }
        }
        //if (expected_index[i] == -1) printf("\nERRRORORORO\n");
    }

    printf(GRN "Type the following:\t\"" MAG "%s" GRN "\"\nIn Morse Code:\t\t\"", word);
    printf(MAG "%s", other_morse_table[expected_index[0]]);
    for (int i = 1; i < 4; i++) {
        printf(BLU "_");
        printf(MAG "%s", other_morse_table[expected_index[i]]);
    }
    printf(GRN "\"\n");
    
    int morse_index[4];
    printf(CYN);
    printf(">>>\t");
    printf(BLU);
    for (int i = 0; i < 3; i++) {
        watchdog_update();
        wait_for_input();
        printf(CYN "_" BLU);
        morse_index[i] = morse_to_index(return_input());
    }
    watchdog_update();
    wait_for_input();
    watchdog_update();
    morse_index[3] = morse_to_index(return_input());
    printf(COLOR_RESET);
    printf("\n");

    char user_word[4];

    for (int i = 0; i < 4; i++) {
        user_word[i] = *morse_corr_table[morse_index[i]];
    }
    printf("You typed: %s\n", user_word);
    correct = are_the_same(expected_index, morse_index);
}

void level_four() {
    const char *word = four_letter_words[(rand() % 500)];

    int expected_index[4];
    for (int i = 0; i < 4; i++) {
        expected_index[i] = -1;
        for (int j = 0; j < 35; j++) {
            if (*morse_corr_table[j] == word[i]) {
                expected_index[i] = j;
                break;
            }
        }
        //if (expected_index[i] == -1) printf("\nERRRORORORO\n");
    }

    printf(GRN "Type the following:\t\"" MAG "%s" GRN "\"\nNo Morse Code here :)\n", word);
    
    int morse_index[4];
    printf(CYN);
    printf(">>>\t");
    printf(BLU);
    for (int i = 0; i < 3; i++) {
        watchdog_update();
        wait_for_input();
        printf(CYN "_" BLU);
        morse_index[i] = morse_to_index(return_input());
    }
    watchdog_update();
    wait_for_input();
    watchdog_update();
    morse_index[3] = morse_to_index(return_input());
    printf(COLOR_RESET);
    printf("\n");

    char user_word[4];

    for (int i = 0; i < 4; i++) {
        user_word[i] = *morse_corr_table[morse_index[i]];
    }
    printf("You typed: %s\n", user_word);
    correct = are_the_same(expected_index, morse_index);
}

void play_level() {
    watchdog_update();
    printf(BLU "\n\t||"COLOR_RESET"   %u/5 Correct "BLU   "|| " COLOR_RESET, num_correct_in_level);
    print_color_from_lives();
    printf("%u", current_lives);
    printf(COLOR_RESET "/3" COLOR_RESET " Lives Remaining" BLU "   ||\n\n" COLOR_RESET);

    switch (current_level)
    {
    case 4:
        level_four();
        break;
    case 3:
        level_three();
        break;
    case 2:
        level_two();
        break;
    case 1:
        level_one();
        break;
    default:
        break;
    }

    if (is_correct()) {
        num_correct_in_level++;
        num_total_correct++;
        if (current_lives < 3) {
            current_lives++;
            LED_color_from_lives();
        }
        if (num_correct_in_level >= 5) {
            num_correct_in_level = 0;
            current_level++;
            print_level_num();
        }
    }
    else {
        current_lives--;
        num_total_incorrect++;
        LED_color_from_lives();
    }
}

void play_levels() {
    print_level_num();
    current_lives = 3;
    num_correct_in_level = 0;
    LED_color_from_lives();
    
    while (current_level <= 4 && current_lives > 0) {
        play_level();
    }
    if (current_lives == 0) {
        print_game_over();
    }
    else {
        print_you_win();
    }
}
