#ifndef TXT_TO_PRINT_H
#define TXT_TO_PRINT_H

extern const char *txt_start_screen;
extern const char *txt_game_over;
extern const char *txt_you_win;
extern const char *txt_level;

extern const char *txt_one;
extern const char *txt_two;
extern const char *txt_three;
extern const char *txt_four;

extern const char *txt_instructions;
extern const char *txt_select_level;

void print_you_win();
void print_game_over();

#endif
