#include <stdbool.h>

// Header guard to prevent multiple inclusion of the header file
#ifndef MORSE_H
#define MORSE_H

// Declaration of external arrays
extern const char *morse_corr_table[];
extern const char *four_letter_words[];
extern const char *morse_table[];
extern const char *other_morse_table[];

// Function declarations
int find_morse_iter(char *input);
int morse_to_index(char *input);
bool find_character_from_iter(char *input);

#endif // MORSE_H
