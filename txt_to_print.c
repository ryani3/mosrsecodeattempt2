#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "txt_to_print.h"
#include "ANSI-color-codes.h"

const char *txt_start_screen = GRN
    "\t=====================================================\n"
    "\t||  "COLOR_RESET"   Assignment 02   "GRN"   ||    "COLOR_RESET"   Group 19    "GRN"    ||\n"
    GRN
    "\t=====================================================\n"
    "\t    _/                                               \n"
    "\t   _/          _/_/      _/_/_/  _/  _/_/  _/_/_/    \n"
    "\t  _/        _/_/_/_/  _/    _/  _/_/      _/    _/   \n"
    "\t _/        _/        _/    _/  _/        _/    _/    \n"
    "\t_/_/_/_/    _/_/_/    _/_/_/  _/        _/    _/     \n"
    "\n"
    "\t    _/      _/                                       \n"
    "\t   _/_/  _/_/    _/_/    _/  _/_/    _/_/_/    _/_/  \n"
    "\t  _/  _/  _/  _/    _/  _/_/      _/_/      _/_/_/_/ \n"
    "\t _/      _/  _/    _/  _/            _/_/  _/        \n"
    "\t_/      _/    _/_/    _/        _/_/_/      _/_/_/   \n"  
    "\n"
    "\t=====================================================\n" 
    COLOR_RESET;

const char *txt_game_over = RED
    "\t==============================================\n"
    "\t  _|_|_|                                      \n"
    "\t_|          _|_|_|  _|_|_|  _|_|      _|_|    \n"
    "\t_|  _|_|  _|    _|  _|    _|    _|  _|_|_|_|  \n"
    "\t_|    _|  _|    _|  _|    _|    _|  _|        \n"
    "\t  _|_|_|    _|_|_|  _|    _|    _|    _|_|_|  \n"
    "\n"
    "\t  _|_|                                        \n"
    "\t_|    _|  _|      _|    _|_|    _|  _|_|      \n"
    "\t_|    _|  _|      _|  _|_|_|_|  _|_|          \n"
    "\t_|    _|    _|  _|    _|        _|            \n"
    "\t  _|_|        _|        _|_|_|  _|            \n"
    "\n"
    "\t==============================================\n"
    COLOR_RESET;

const char *txt_you_win = BLU
    "\t================================================================\n"
    "\t_|      _|                        _|          _|  _|            \n"
    "\t  _|  _|    _|_|    _|    _|      _|          _|      _|_|_|    \n"
    "\t    _|    _|    _|  _|    _|      _|    _|    _|  _|  _|    _|  \n"
    "\t    _|    _|    _|  _|    _|        _|  _|  _|    _|  _|    _|  \n"
    "\t    _|      _|_|      _|_|_|          _|  _|      _|  _|    _|  \n"
    "\t================================================================\n"
    COLOR_RESET;

const char *txt_level = GRN
    "\t===================================================\n"
    "\t    _/                                        _/   \n"
    "\t   _/          _/_/    _/      _/    _/_/    _/    \n"
    "\t  _/        _/_/_/_/  _/      _/  _/_/_/_/  _/     \n"
    "\t _/        _/          _/  _/    _/        _/      \n"
    "\t_/_/_/_/    _/_/_/      _/        _/_/_/  _/       \n"
    "\n";

const char *txt_one = GRN
    "\t                        _|  \n"
    "\t                      _|_|  \n"
    "\t                        _|  \n"
    "\t                        _|  \n"
    "\t                        _|  \n";

const char *txt_two = GRN
    "\t                       _|_|    \n"
    "\t                     _|    _|  \n"
    "\t                         _|    \n"
    "\t                       _|      \n"
    "\t                     _|_|_|_|  \n";

const char *txt_three = YEL
    "\t                     _|_|_|    \n"
    "\t                           _|  \n"
    "\t                       _|_|    \n"
    "\t                           _|  \n"
    "\t                     _|_|_|    \n";

const char *txt_four = RED
    "\t                     _|  _|    \n"
    "\t                     _|  _|    \n"
    "\t                     _|_|_|_|  \n"
    "\t                         _|    \n"
    "\t                         _|    \n";

const char *txt_instructions = YEL
"--- HOW TO PLAY--- \n"
"\n"
"A chracter or word will be prompted on screen\n"
"You are required to type the character or word in morse code\n"
"Only one button is used: The GP21 button on the Raspberry Pi\n"
"\n"
"Quickly tap the button to produce a dot (\"∙\")\n"
"Hold the button for at least a quarter of a second to produce a dash (\"-\")\n"
"After five presses, or a short pause after the last press, a space is produced (\"_\")\n"
"\n"
"Choose a level. Beating one increments you to the next level\n"
"Correctly type the morse code five times to beat the level\n"
"You have three chances (lives) to make a mistake. You can get a life back by typing correctly\n"
"\n"
"Beating the highest level wins the game\n\n"
COLOR_RESET;

const char *txt_select_level = GRN
    "Type the level number to play!\n"
    "\tLevel 1 -> ∙----\n"
    "\tLevel 2 -> ∙∙---\n"
    YEL
    "\tLevel 3 -> ∙∙∙--\n"
    RED
    "\tLevel 4 -> ∙∙∙∙-"
    COLOR_RESET;



void print_welcome_screen(){

  printf(GRN "\n██╗     ███████╗ █████╗ ██████╗ ███╗   ██╗    ███╗   ███╗ ██████╗ ██████╗ ███████╗███████╗ \n");
  printf("██║     ██╔════╝██╔══██╗██╔══██╗████╗  ██║    ████╗ ████║██╔═══██╗██╔══██╗██╔════╝██╔════╝ \n");
  printf("██║     █████╗  ███████║██████╔╝██╔██╗ ██║    ██╔████╔██║██║   ██║██████╔╝███████╗█████╗   \n");
  printf("██║     ██╔══╝  ██╔══██║██╔══██╗██║╚██╗██║    ██║╚██╔╝██║██║   ██║██╔══██╗╚════██║██╔══╝   \n");
  printf("██║     ██╔══╝  ██╔══██║██╔══██╗██║╚██╗██║    ██║╚██╔╝██║██║   ██║██╔══██╗╚════██║██╔══╝   \n");
  printf("███████╗███████╗██║  ██║██║  ██║██║ ╚████║    ██║ ╚═╝ ██║╚██████╔╝██║  ██║███████║███████╗ \n");
  printf("╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝    ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ \n");
  printf(GRN "||  "COLOR_RESET"   Assignment 02   "GRN"   ||    "COLOR_RESET"   Group 19    "GRN"    ||\n");
  printf(COLOR_RESET);
  printf("\n");
 
  

}

void print_game_over(){
  printf(RED);
  printf("\n   ▄████  ▄▄▄       ███▄ ▄███▓▓█████     ▒█████   ██▒   █▓▓█████  ██▀███ \n");
  printf(" ██▒ ▀█▒▒████▄    ▓██▒▀█▀ ██▒▓█   ▀    ▒██▒  ██▒▓██░   █▒▓█   ▀ ▓██ ▒ ██▒\n");
  printf("▒██░▄▄▄░▒██  ▀█▄  ▓██    ▓██░▒███      ▒██░  ██▒ ▓██  █▒░▒███   ▓██ ░▄█ ▒\n");
  printf("░▓█  ██▓░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄    ▒██   ██░  ▒██ █░░▒▓█  ▄ ▒██▀▀█▄  \n");
  printf("░▒▓███▀▒ ▓█   ▓██▒▒██▒   ░██▒░▒████▒   ░ ████▓▒░   ▒▀█░  ░▒████▒░██▓ ▒██▒\n");
  printf(" ░▒   ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░   ░ ▒░▒░▒░    ░ ▐░  ░░ ▒░ ░░ ▒▓ ░▒▓░\n");
  printf("  ░   ░   ▒   ▒▒ ░░  ░      ░ ░ ░  ░     ░ ▒ ▒░    ░ ░░   ░ ░  ░  ░▒ ░ ▒░\n");
  printf("░ ░   ░   ░   ▒   ░      ░      ░      ░ ░ ░ ▒       ░░     ░     ░░   ░ \n");
  printf("      ░       ░  ░       ░      ░  ░       ░ ░        ░     ░  ░   ░     \n");
  printf("                                                     ░                   \n");
  printf(COLOR_RESET); 
}


void print_you_win(){
  printf(BLU "\n▄██   ▄    ▄██████▄  ███    █▄        ▄█     █▄   ▄█  ███▄▄▄▄  \n");
  printf("███   ██▄ ███    ███ ███    ███      ███     ███ ███  ███▀▀▀██▄ \n");
  printf("███▄▄▄███ ███    ███ ███    ███      ███     ███ ███▌ ███   ███  \n");
  printf("▀▀▀▀▀▀███ ███    ███ ███    ███      ███     ███ ███▌ ███   ███  \n");
  printf("▄██   ███ ███    ███ ███    ███      ███     ███ ███▌ ███   ███ \n");
  printf("███   ███ ███    ███ ███    ███      ███     ███ ███  ███   ███ \n");
  printf("███   ███ ███    ███ ███    ███      ███ ▄█▄ ███ ███  ███   ███  \n");
  printf(" ▀█████▀   ▀██████▀  ████████▀        ▀███▀███▀  █▀    ▀█   █▀ \n");
  printf(COLOR_RESET);
}
