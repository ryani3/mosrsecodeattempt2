#include "hardware/regs/timer.h"
#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/m0plus.h"

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.global wait_for_input
.global return_input
.align  4

@ LED
.equ    LED_VAL_ON,    1           @ Specify value that turns the LED "on"
.equ    LED_VAL_OFF,   0           @ Specify value that turns the LED "off"
.equ    DFLT_STATE_STRT, 1         @ Specify the value to start flashing
.equ    DFLT_STATE_STOP, 0         @ Specify the value to stop flashing

@ BTN
.equ    GPIO_BTN_EN,  21           @ Specify pin for the "enter" button
.equ    GPIO_DIR_IN,   0           @ Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,  1           @ Specify output direction for a GPIO pin
.equ    GPIO_LED_PIN, 25           @ Specify pin for the built-in LED
.equ    GPIO21_FALLING, 0x00400000 @ Bit-22 gpio_falling-edge on GP21
.equ    GPIO21_RISING, 0x00800000  @ Bit-23 gpio_rising-edge on GP21
.equ    GPIO_ISR_OFFSET, 0x74      @ GPIO is 13 in vector table 29

@ ALARM
.equ    ALRM_ISR_OFFSET, 0x40      @ ALARM0 is 0 in vector table 16
.equ    DFLT_ALARM_TIME, 1500000   @ Specify the default alarm timeout
.equ    FAST_ALARM_TIME, 0100000   @ Specify the fast alarm timeout

@ DASHES AND DOTS
.equ    DASH, 0x2D                 @ assigning ASCII value for "-"
.equ    DOT, 0x2E                  @ assigning ASCII value for "."
.equ    END_STRING, 0x0            @ assigning ASCII value for "\0"
.equ    DASH_THRESHOLD, 220000     @ waiting time for dash 

main_asm:
    push  {r0-r7, lr}              @ Push variables to stack and Link Register
    bl    start_btn                @ Entry point for "start button" function 
    bl    start_led                @ Entry point for "Start Led" function 
    bl    start_gpio_isr           @ Entry point for "GPIO start" function
    bl    start_alarm_isr          @ Entry point for "Alarm start" function 
    pop   {r0-r7, pc}              @ Pop values from stack and Program Counter

start_btn:
    push    {lr}                   @ Push Link Register 
    movs    r0, #GPIO_BTN_EN       @ Enable button   
    bl      asm_gpio_init          @ GPIO initialisation subroutine 
    movs    r0, #GPIO_BTN_EN       @ Moving Button enable value to register R0 
    movs    r1, GPIO_DIR_IN        @ Moving Input Direction for a GPIO pin to R1 
    bl      asm_gpio_set_dir       @ Subroutine for setting the GPIO 
    movs    r0, #GPIO_BTN_EN       @ Moving pin enter value to R0 
    bl      asm_gpio_set_irq       @ TODO  
    pop {pc}                       @ Pop programm couter 

start_led:
    push    {lr} 
    movs    r0, #GPIO_LED_PIN           @ This value is the GPIO LED pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_LED_PIN           @ This value is the GPIO LED pin on the PI PICO board
    movs    r1, #GPIO_DIR_OUT           @ We want this GPIO pin to be setup as an output pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1
    pop     {pc} 

start_alarm_isr:
    ldr     r1, =(PPB_BASE + M0PLUS_VTOR_OFFSET) @ Load vector table
    movs    r0, #1                      @ Move the ON value to the register R0 
    ldr     r0, [r1]                    @ Load the ON value to the calculated Value 
    movs    r1, #ALRM_ISR_OFFSET        @ Put it into the calculated alarm offset value 
    add     r1, r0                      @ Go to alarm isr offset                   
    ldr     r0, =alarm_isr              @ TODO              
    str     r0, [r1]                    @ Store the alarm value                      
    movs    r0, #1                      @ Move the 
    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET) @ Disable IRQ
    str     r0, [r1]                    @              
    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET) @ Enable IRQ
    str     r0, [r1]                    @ 
    ldr     r1, =(TIMER_BASE + TIMER_INTE_OFFSET) @ Disable ISR
    movs    r0, #0                      @ 
    str     r0, [r1]                    @ store value 0 into the vector 
    bx      lr                          @ 

start_gpio_isr:
    ldr     r1, =(PPB_BASE + M0PLUS_VTOR_OFFSET) @ Get the RAM vector table address
    ldr     r0, [r1]
    movs    r1, #GPIO_ISR_OFFSET                      
    add     r1, r0  @ Add the offset for the GPIO ISR event
    ldr     r0, =gpio_isr @ Hook it to this function
    str     r0, [r1] 

    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET) @ Disable IRQ                            
    ldr     r0, =0x2000
    str     r0, [r1] 

    ldr     r1, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET) @ Enable IRQ                                                
    str     r0, [r1] 
    bx      lr  

.thumb_func
alarm_isr:
    push    {lr}
    movs    r4, #1
    ldr     r5, =(TIMER_BASE + TIMER_INTR_OFFSET) @ Acknoledge ISR event
    str     r4, [r5]
    bl send_input
    pop     {pc} 

.thumb_func
gpio_isr:
    push {lr}       
    bl asm_watchdog_update @ Prevents the device from resetting due to timeout    
    ldr     r4, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET) @ Check the event status
    ldr     r3, [r4]   	                                      
    ldr     r4, =GPIO21_FALLING                               
    cmp     r4, r3                                            
    beq gpio_rising             @ If the interrupt status was falling, go to rising
    ldr     r4, =GPIO21_RISING                                
    cmp     r4, r3                                            
    beq gpio_falling            @ If rising, go to falling
    bl end_gpio_isr
.thumb_func
gpio_rising: 
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)      @ Reset the status
    ldr     r1,=GPIO21_FALLING                                
    str     r1,[r2]                                           
    ldr     r2, =(TIMER_BASE + TIMER_TIMELR_OFFSET)           
    ldr     r6, [r2]
    b end_gpio_isr
.thumb_func
gpio_falling:
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)      @ Reset the status
    ldr     r1,=GPIO21_RISING                                 
    str     r1,[r2]                                           

    ldr     r2, =(TIMER_BASE + TIMER_TIMELR_OFFSET)           
    ldr     r7, [r2]

    subs    r0, r7, r6                                        
    ldr     r1, =DASH_THRESHOLD                               
    cmp     r0, r1                                            
    bhs dash 
.thumb_func
dot: 
    ldr     r0, =dot_message
    bl printf
    ldr     r0, =morse_code_array 
    ldr     r1, =morse_code_iter  
    ldr     r2, [r1]   
    adds    r0, r2     
    ldr     r1, =DOT  @ Load a "."
    str     r1, [r0]   
    bl iterate  @ Skip dash
.thumb_func
dash: 
    ldr     r0, =dash_message
    bl printf
    ldr     r0, =morse_code_array 
    ldr     r1, =morse_code_iter  
    ldr     r2, [r1]   
    adds    r0, r2     
    ldr     r1, =DASH  @ Load a "-"
    str     r1, [r0]   
.thumb_func
iterate:
    ldr     r1, =morse_code_iter  
    ldr     r2, [r1]   
    adds    r2, #4  
    str     r2, [r1]

    movs    r0, #20  
    cmp     r2, r0
    bge     max_iteration
    bl set_alarm
    b end_gpio_isr

.thumb_func
max_iteration:
    bl      skip_alarm
.thumb_func
end_gpio_isr:
    pop {pc}

.thumb_func
skip_alarm:
    ldr     r1, =(TIMER_BASE + TIMER_TIMELR_OFFSET) @ Load the current time
    ldr     r0, [r1]            
    ldr     r1, =FAST_ALARM_TIME @ Add the alarm time
    add     r0, r1
    ldr     r1, =(TIMER_BASE + TIMER_ALARM0_OFFSET)
    str     r0, [r1]                              @ Set the new time for alarm
    ldr     r1, =(TIMER_BASE + TIMER_INTE_OFFSET) @ Enable the alarm
    movs    r0, #1
    str     r0, [r1]
    bx      lr  

.thumb_func
set_alarm:
    ldr     r1, =(TIMER_BASE + TIMER_TIMELR_OFFSET) @ Load the current time
    ldr     r0, [r1]            
    ldr     r1, =DFLT_ALARM_TIME @ Add the alarm time
    add     r0, r1
    ldr     r1, =(TIMER_BASE + TIMER_ALARM0_OFFSET)
    str     r0, [r1]                              @ Set the new time for alarm
    ldr     r1, =(TIMER_BASE + TIMER_INTE_OFFSET) @ Enable the alarm
    movs    r0, #1
    str     r0, [r1]
    bx      lr  


.thumb_func
wait_for_input:
    push {r0-r7, lr}               @ Push registers to the stack
    ldr r7, =0                     @ Load 

    ldr     r1, =morse_code_iter   @ Reset iter to 0
    ldr     r0, =END_STRING        @ Load value '\0'
    str     r0, [r1] @ Reset iteration

.thumb_func                          
wait_for_input_loop:               @ Starting the waiting subroutine 
    wfi                            @ Wait For Interrupt 
    cmp r7, #1                     @ Compare Register
    bne wait_for_input_loop        @ Loop the waiting subroutine                 
    pop {r0-r7, pc}                @ Pop registers and the Program Counter


.thumb_func
send_input:
    ldr     r1, =morse_code_iter    @ Load iter
    ldr     r0, [r1]                @ Load the iteration 
    ldr     r1, =morse_code_array   @ Load array
    adds    r1, r0                  @ Add iter to array
    ldr     r0, =END_STRING         @ Load "/0" to location
    str     r0, [r1]                @ Store the end of the string 

    ldr     r1, =morse_code_iter   @ Reset iter to 0
    ldr     r0, =END_STRING         
    str     r0, [r1]               @ Reset iteration

    ldr     r1, =(TIMER_BASE + TIMER_INTE_OFFSET) @ End of acknowlegdement
    movs    r0, #0    
    str     r0, [r1]                @ Make String
    ldr r7, =1                      @ End wait_for_input_loop
    pop {pc}                        @ Pop Program counter
.thumb_func
return_input:
    push {lr}
    ldr r0, =morse_code_array       @ Return the inputs made by user.
    pop {pc}

.align 4
dot_message: .asciz "∙"
dash_message: .asciz "-"    

.data
morse_code_array: .skip 24
morse_code_iter: .word 0
lstate: .word   DFLT_STATE_STRT
ltimer: .word   DFLT_ALARM_TIME

.end
